#!/usr/bin/env python3

"""
    VIN Decode
    Process an input vin and setup to do a range of sequential
    plant numbers for the same truck configuration.

    Copyright (C) July 2020, Michael Greene <mikeos2@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

"""


from vin.vindecode import VinDecode
from vin.checkbuild import CheckBuild

# https://www.jeep.com/webselfservice/BuildSheetServlet?vin=1C6SRFFTXLN403551
# https://www.jeep.com/webselfservice/BuildSheetServlet?vin=1C6SRFFT0LN403574
# https://www.jeep.com/webselfservice/BuildSheetServlet?vin=1C6SRFFT1LN404670
# Sticker: https://www.ramtrucks.com/hostd/windowsticker/getWindowStickerPdf.do?vin=1C6SRFFT1LN404670
# https://www.jeep.com/webselfservice/BuildSheetServlet?vin=1C6SRFFT5LN404686
# Sticker: https://www.ramtrucks.com/hostd/windowsticker/getWindowStickerPdf.do?vin=1C6SRFFT5LN404686



# the following is a check to verify working
# put a valid VIN you want to probe for
# i.e the first 8 elements and element 10 and 11
vin = VinDecode("1C6SRFFT8LN404956")
bld = CheckBuild()
if vin.vinchecked():
    print("Input VIN checked correct")
else:
    print("Bad VIN checksum for input!")

# start = 404686
start = 404700

loops = 300
count1 = 0
count2 = 0

for x in range(start, start+loops):
    result = bld.getvinsize(vin.getfullvin(x))
    if result is not "dud":
        print(result)
        count1 += 1
    result = bld.getvinsize(vin.getfullvin(x), True)
    if result is not "dud":
        print("Sticker: " + result)
        count2 += 1

print("Found " + str(count1) + " sheets out of " + str(loops) + " checks.")
print("Found " + str(count2) + " stickers out of " + str(loops) + " checks.")
