#!/usr/bin/env python3

"""
    VIN Decode
    Process an input vin and setup to do a range of sequential
    plant numbers for the same truck configuration.

    Copyright (C) July 2020, Michael Greene <mikeos2@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    VinDecode --> input valid VIN
        - decode each element using pos_weight table
        - find specific weighting in weight table and multiply with decoded element
        - sum all elements and divide by 11, remainder is checksum
        - if calculated checksum matches input checksum, vinisgood set True
"""


class VinDecode:
    """ Class to decode vin."""

    def __init__(self, startvin):
        # save the starting VIN
        self.startvin = startvin
        # hold processed VIN elements
        self.decoded = []
        # save the start plant sequence number
        self.startseq = int(startvin[-6:])
        # vinisgood if the load checksum matches input checksum
        self.vinisgood = False
        # truck model/type constant number
        self.constant = 0
        # VIN strings for new calc
        self.curr_modelid = self.startvin[0:-9]
        self.curr_plantid = self.startvin[9:-6]

        # ****** Lookup tables *******
        self.pos_weight = {
            '0': '0', '1': '1', '2': '2', '3': '3', '4': '4', '5': '5', '6': '6', '7': '7', '8': '8',
            '9': '9', 'A': '1', 'B': '2', 'C': '3', 'D': '4', 'E': '5', 'F': '6', 'G': '7', 'H': '8',
            'J': '1', 'K': '2', 'L': '3', 'M': '4', 'N': '5', 'P': '7', 'R': '9', 'S': '2', 'T': '3',
            'U': '4', 'V': '5', 'W': '6', 'X': '7', 'Y': '8', 'Z': '9'
        }

        self.weight = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2]

        # some temps
        vincheck = 0  # hold total for load VIN check
        count = 0  # index counter
        checksum = 0  # input checksum save

        # breakup vin into a list
        for vin_element in startvin:
            # if this is checksum position, handle it
            if count is 8:
                checksum = int(vin_element)
                vin_element = '0'
            # decode element and multiply by element weight
            self.decoded.append(int(self.pos_weight[vin_element]) * self.weight[count])
            # sum for load VIN check
            vincheck += self.decoded[count]
            count += 1
        # Check calculated VIN against input checksum
        if (vincheck % 11) is checksum:
            self.vinisgood = True
        # the checksum position in decode list should be zero
        # so setup truck model/type constant
        for x in range(0, 11):
            self.constant += self.decoded[x]

    # load and check status
    def vinchecked(self):
        return self.vinisgood

    def getfullvin(self, seqnumber):
        # needs to be string
        seqnumber = str(seqnumber)
        # rough error check
        if len(seqnumber) > 6:  # ToDo len < 6
            return "Bad sequence number!"
        # get seq number weghting
        seq_sum = 0
        count = 11
        for seq_element in seqnumber:
            seq_sum += int(seq_element) * self.weight[count]
            count += 1
        # calculate checksum and handle checksum of 10
        checksum = (seq_sum + self.constant) % 11
        if checksum is 10:
            checksum = 'X'
        else:
            checksum = str(checksum)
        # return full VIN
        return self.curr_modelid + checksum + self.curr_plantid + seqnumber
