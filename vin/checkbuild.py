#!/usr/bin/env python3

"""
    Check Build
    Given a valid VIN, return the url to a RAM (Jeep?) build
    sheet if exists.

    Copyright (C) July 2020, Michael Greene <mikeos2@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Takes a valid VIN as input and uses urllib to get page header to extract
    pdf size usually about 1.2k to 1.5k is an invalid VIN page. A valid VIN
    build sheet is 8-9k, so a good hit returns the valid url. A False return
    for a blank VIN page.
"""

import urllib3


class CheckBuild:
    """ Class to get build sheet size"""

    def __init__(self):
        self.url1 = "https://www.jeep.com/webselfservice/BuildSheetServlet?vin="
        self.url2 = "https://www.ramtrucks.com/hostd/windowsticker/getWindowStickerPdf.do?vin="
        self.http = urllib3.PoolManager()

    def getvinsize(self, lookup, sticker = False):
        """ check for valid build sheet or sticker"""
        # sticker false - check for build sheet
        # sticker true - check for window sticker
        if sticker is False:
            curr_url = self.url1 + lookup
        else:
            curr_url = self.url2 + lookup
        resp = self.http.request('HEAD', curr_url)
        size = int(resp.headers['Content-Length'])
        if size > 1130:
            return curr_url
        else:
            return "dud"
