A simple python script I used when I ordered a new RAM truck. 

Edit getvin.py and replace the VIN, in the following statement, with your VIN:
vin = VinDecode("1C6SRFFT8LN404956") 

Find the following statements:
start = 401528
loops = 3428

Set start to the sequence number to start at and loops to the number of sequence numbers to check

After running, it should spit out output:

https://www.jeep.com/webselfservice/BuildSheetServlet?vin=1C6SRFFT2LN398992
https://www.jeep.com/webselfservice/BuildSheetServlet?vin=1C6SRFFT5LN400749
https://www.jeep.com/webselfservice/BuildSheetServlet?vin=1C6SRFFT6LN400971
https://www.jeep.com/webselfservice/BuildSheetServlet?vin=1C6SRFFT3LN400975